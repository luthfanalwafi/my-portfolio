# My Portfolio

This is all of my project, include my personal projects and contributed projects

## Personal Project

repository  : https://gitlab.com/luthfanalwafi/laku-apps-project

playstore   : https://play.google.com/store/apps/details?id=com.laku.kainprinting

appstore    : https://apps.apple.com/id/app/kainprinting/id1460654977

----------

repository  : https://gitlab.com/luthfanalwafi/alwafi-scoreboard

apk         : -

----------

repository  : https://gitlab.com/luthfanalwafi/alwafi-project

apk         : -

## Contributed Project

repository  : private

playstore   : https://play.google.com/store/apps/details?id=com.icon.pln123

appstore    : https://apps.apple.com/nz/app/pln-mobile/id1299581030

----------

repository  : https://gitlab.com/swamedia.developer/jurnal-ramadhanqu

playstore   : https://play.google.com/store/apps/details?id=com.swamedia.jurnalramadhanqu
